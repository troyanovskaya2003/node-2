const express = require('express');
const router = express.Router();
const { createNote, getNotes, getNote, deleteNote, updateNote, changeCheckNote} = require('./notesService.js');
const {authMiddleware}=require('./middleware/authMiddleware.js');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getNote);

router.delete('/:id', authMiddleware,  deleteNote);

router.put('/:id', authMiddleware, updateNote);
router.patch('/:id', authMiddleware, changeCheckNote)

module.exports = {
  notesRouter: router
};
