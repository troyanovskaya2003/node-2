const jwt=require('jsonwebtoken');

function authMiddleware(req, res, next){
    const {authorization}=req.headers;
    if(!authorization){
        return res.status(401).json({"message":"Please, provide valid header"});
    }
    const [, token]=authorization.split(' ');
    if(!token){
        next(new Error('Please, include token to request'));
    }
    try{
        const tokenPayLoad=jwt.verify(token, 'secret-key');
        req.user={userId:tokenPayLoad.userId, username:tokenPayLoad.username};
        next()
    }catch(e){
        res.status(401).json({message: err.message});
    }
}



module.exports={
    authMiddleware
}
