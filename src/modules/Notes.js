const mongoose=require('mongoose');
mongoose.connect('mongodb+srv://Ann:fndt75DSk@cluster0.4skxzza.mongodb.net/ToDoApp?retryWrites=true&w=majority');

const noteSchema=mongoose.Schema({
  userId:{
    type:String,
    required:true
  },
  completed:{
    type:Boolean,
    required:true
  },
  text:{
    type:String,
    required:true
  },
  createdDate:{
    type:String,
    required:true
  }
});

const Note=mongoose.model('Note', noteSchema);
module.exports={
    Note
}