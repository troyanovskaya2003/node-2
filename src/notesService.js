const {Note}=require('./modules/Notes.js');
const {User}=require('./modules/Users.js');

async function createNote (req, res, next) {
  try{  
    const user=await User.findById(req.user.userId);
    let {text}=req.body;
    let userId =user._id;
    let completed=false;
    let createdDate= JSON.stringify(new Date());
    if(text){
      const note= await new Note({userId, completed, text, createdDate});
      note.save().then(data=>console.log(data));
      res.status(200).send({ "message": "Success" });
    }else{
      res.status(400).send({ "message": "string" });
    }

  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }

}

//Get user's notes
async function getNotes (req, res, next) {
  try{
    let offset=0;
    let limit=0;
    if (req.query){
      const offset=req.query.offset;
      const limit=req.query.limit;
    }
    const user=await User.findById(req.user.userId);
    const notes=await Note.find({"userId":user._id});
    if(limit==0){
      limit=notes.length;
    }
    let arr=[]; 
    if(limit<0 || offset<0){
      res.status(400).send({"message":"bad request"});
    }   
    for(let i=0; i<notes.length; i++){
      if(offset){
        offset--;
        continue;
      }
      if(arr.length==limit){
        break;
      }
      arr.push(notes[i]);
    }
    res.status(200).json({ "offset": offset,
    "limit": limit,
    "count": arr.length,
    "notes":arr});
  }catch(e){
    res.status(500).send({ "message": "internal server error"});
  }
}

const getNote = async (req, res, next) => {
  try{  
    const note=await Note.findById(req.params.id);
    if(note.userId==req.user.userId){
      res.status(200).send({ "message": "success", "note":note});
    }else{
      res.status(400).send({ "message": "bad request" });
    }
  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

async function deleteNote(req, res, next){
  try{
    const note = await Note.findById(req.params.id);
    if(req.params.id && note.userId==req.user.userId){
      note.delete();
      res.status(200).send({ "message": "success"});
    }else{
      res.status(400).send({ "message": "string" });
    }
  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

async function updateNote(req, res, next){
  try{  
    if(req.body){
      const note = await Note.findById(req.params.id);
      const {text}=req.body;
      if(note.userId==req.user.userId){
        note.text=text;
        note.save();
        res.status(200).send({ "message": "success"});
      }
    }else{
      res.status(400).send({ "message": "bad request" });
    }

  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}
async function changeCheckNote(req, res, next){
  try{
    if(req.params.id){
      const note = await Note.findById(req.params.id);
      if(note.userId==req.user.userId){
        note.completed=!note.completed;
        note.save();
        res.status(200).send({ "message": "success"});
      }
    }else{
      res.status(400).send({ "message": "bad request" });
    }
  }catch(e){
    res.status(500).send({ "message": "internal server error" });
  }
}

module.exports = {
  createNote,
  getNotes,
  getNote,
  deleteNote,
  updateNote,
  changeCheckNote
}
