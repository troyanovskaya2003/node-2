const mongoose=require('mongoose');
mongoose.connect('mongodb+srv://Ann:fndt75DSk@cluster0.4skxzza.mongodb.net/ToDoApp?retryWrites=true&w=majority');
// const secret='secret-key';
// const webToken=jwt.sign({name:'Annastasiia', id:'34567'}, secret);
// console.log(webToken);
//const verified=jwt.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQW5uYXN0YXNpaWEiLCJpZCI6IjM0NTY3IiwiaWF0IjoxNjYwNjg5NDYwfQ.3D10o2DSu4SdCBqkoNmsXgOdp0KRS3uxrzys8ELgfCg', secret);
//console.log(verified);
// const decoded=jwt.decode('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQW5uYXN0YXNpaWEiLCJpZCI6IjM0NTY3IiwiaWF0IjoxNjYwNjg5NDYwfQ.3D10o2DSu4SdCBqkoNmsXgOdp0KRS3uxrzys8ELgfCg', secret);
// console.log(decoded);

const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();
const port=8080;

const { notesRouter } = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/', usersRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error('err')
  res.status(500).send({'message': 'Server error1'});
}
